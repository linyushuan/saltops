from django.apps import AppConfig


class CeleryManagerConfig(AppConfig):
    name = 'celery_manager'
