# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-26 10:42
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cmdb', '0005_auto_20170826_1804'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rack',
            name='cabinet',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cmdb.Cabinet', verbose_name='机柜'),
        ),
    ]
